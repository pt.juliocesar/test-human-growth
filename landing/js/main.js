$(document).ready(function(){
    $('.slideList').slick({
	   slidesToShow: 1,
	   slidesToScroll: 1,
	   autoplay: true,
	   dots: true,
	   arrows: false,
	   fade: true
	 });
});